import com.s4n.delivery.DroneException;
import com.s4n.delivery.impl.Drone;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class DroneUnitTest {

    public static int MAX_DELIVERY_NUMBER = 3;
    @Test
    public void givenValidRoutes_ExecuteRoute_ShouldSuccess() throws DroneException {
        String[] routes = {"AAAAIAA", "AAIAAIAA", "DAA"};
        List<String> streamRoutes = Arrays.asList(routes);
        Drone drone = new Drone(streamRoutes,MAX_DELIVERY_NUMBER);
        drone.executeRoute();
    }

    @Test(expected = DroneException.class)
    public void givenExceedDeliveries_ExecuteRoute_ShouldFail() throws DroneException {
        String[] routes = {"AAAAAA", "AAIAAIAA", "DAA", "DAA"};
        List<String> streamRoutes = Arrays.asList(routes);
        Drone drone = new Drone(streamRoutes,MAX_DELIVERY_NUMBER);
        drone.executeRoute();
    }
}
