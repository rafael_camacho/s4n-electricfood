import com.s4n.delivery.ZoneException;
import com.s4n.delivery.impl.Drone;
import com.s4n.delivery.impl.Zone;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ZoneUnitTest {

    public static final String TEN_NUMBER_OF_BLOCKS = "10";

    @Test
    public void givenValidRoutes_ValidateCoverage_ShouldSuccess() throws ZoneException {
        String[] routes = {"AAAAIAA", "AAIAAIAA", "DAA"};
        List<String> streamRoutes = Arrays.asList(routes);

        Zone zone = new Zone(TEN_NUMBER_OF_BLOCKS);
        zone.validateCoverage(streamRoutes);
    }

    @Test(expected = ZoneException.class)
    public void givenInvalidRoutes_ValidateCoverage_ShouldFail() throws ZoneException {
        String[] routes = {"AAAAAAAAAAAAAIAA", "AAIAAIAA", "DAA"};
        List<String> streamRoutes = Arrays.asList(routes);
        Drone drone = new Drone(streamRoutes, DroneUnitTest.MAX_DELIVERY_NUMBER);

        Zone zone = new Zone(TEN_NUMBER_OF_BLOCKS);
        zone.validateCoverage(drone.getRoutes());
    }
}
