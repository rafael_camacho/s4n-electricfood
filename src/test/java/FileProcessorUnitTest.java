import com.s4n.delivery.FileProcessorException;
import com.s4n.delivery.impl.InputFileProcessor;
import com.s4n.delivery.impl.OutputFileProcessor;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FileProcessorUnitTest {

    public static final String FILE_IN_PREFIX = "in";

    public static final String FILE_OUT_PREFIX = "out";

    public static final String FILE_EXTENSION = ".txt";

    public static int DRONE_NUMBER = 1;

    public static int DRONE_WRONG_NUMBER = 21;

    @Test
    public void givenValidDroneDataAndFile_ReadFile_ShouldSuccess() throws FileProcessorException {
        InputFileProcessor inputFileProcessor = new InputFileProcessor();
        inputFileProcessor.readFile(FILE_IN_PREFIX,FILE_EXTENSION,DRONE_NUMBER, DroneUnitTest.MAX_DELIVERY_NUMBER);
    }

    @Test(expected = FileProcessorException.class)
    public void givenInvalidDroneData_ReadFile_ShouldFail() throws FileProcessorException {
        InputFileProcessor inputFileProcessor = new InputFileProcessor();
        inputFileProcessor.readFile(FILE_IN_PREFIX,FILE_EXTENSION,DRONE_WRONG_NUMBER, DroneUnitTest.MAX_DELIVERY_NUMBER);
    }

    @Test
    public void givenValidReport_writeDelivery_ShouldSuccess() throws FileProcessorException, IOException {
        List<String> routeList = Arrays.asList("(-2, 4) dirección Norte","(-3, 3) dirección Sur","(-4, 2) dirección Oriente");
        OutputFileProcessor outputFileProcessor = new OutputFileProcessor();
        outputFileProcessor.writeDelivery(routeList, FILE_OUT_PREFIX,FILE_EXTENSION,DRONE_NUMBER);
    }

}
