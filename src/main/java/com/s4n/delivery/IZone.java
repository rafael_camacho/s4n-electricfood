package com.s4n.delivery;

import java.util.List;

public interface IZone {

    void validateCoverage(List<String> routes) throws ZoneException;
}
