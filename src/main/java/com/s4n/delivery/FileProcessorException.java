package com.s4n.delivery;

public class FileProcessorException extends Exception {

    public FileProcessorException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
