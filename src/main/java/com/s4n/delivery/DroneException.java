package com.s4n.delivery;

public class DroneException extends Exception {

    public DroneException(String errorMessage){
        super(errorMessage);
    }
}
