package com.s4n.delivery;

public class ZoneException extends Exception {

    public ZoneException(String s) {
        super(s);
    }
}
