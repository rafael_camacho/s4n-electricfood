package com.s4n.delivery;

import java.util.List;

public interface IOutputFileProcessor {

    void writeDelivery(List<String> deliveryReport,String filePrefix, String fileExtension, int droneNumber) throws FileProcessorException;
}
