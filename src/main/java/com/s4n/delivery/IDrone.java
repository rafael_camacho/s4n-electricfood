package com.s4n.delivery;

import java.util.List;

public interface IDrone {

    List<String> executeRoute() throws DroneException;
}
