package com.s4n.delivery;

import com.s4n.delivery.impl.Drone;

public interface IInputFileProcessor {

    Drone readFile(String filePrefix, String fileExtension, int droneNumber, int maxDeliveryNumber) throws FileProcessorException;
}
