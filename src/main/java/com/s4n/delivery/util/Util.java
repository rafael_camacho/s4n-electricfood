package com.s4n.delivery.util;

public class Util {

    public static String getOrientation(char character, String orientation){
        if (character == 'I' && orientation.equalsIgnoreCase("N")) {
            orientation = "O";
        }
        else if(character == 'D' && orientation.equalsIgnoreCase("N")) {
            orientation = "E";
        }
        else if (character == 'I' && orientation.equalsIgnoreCase("O")) {
            orientation = "S";
        }
        else if (character == 'D' && orientation.equalsIgnoreCase("O")) {
            orientation = "N";
        }
        else if (character == 'I' && orientation.equalsIgnoreCase("S")) {
            orientation = "E";
        }
        else if (character == 'D' && orientation.equalsIgnoreCase("S")) {
            orientation = "O";
        }
        else if (character == 'I' && orientation.equalsIgnoreCase("E")) {
            orientation = "N";
        }
        else if (character == 'D' && orientation.equalsIgnoreCase("E")) {
            orientation = "S";
        }
        return orientation;
    }

    public static String getFullOrientation(String orientation){
        String result = "";
        if (orientation.equalsIgnoreCase("N")){
            result = "Dirección Norte";
        }
        else if(orientation.equalsIgnoreCase("E")){
            result = "Dirección Este";
        }
        else if(orientation.equalsIgnoreCase("O")){
            result = "Dirección Oriente";
        }
        else if(orientation.equalsIgnoreCase("S")){
            result = "Dirección Sur";
        }
        return result;
    }
}
