package com.s4n.delivery.impl;

import com.s4n.delivery.IZone;
import com.s4n.delivery.ZoneException;
import com.s4n.delivery.util.Util;

import java.util.List;

public class Zone implements IZone {

    private int blocks;

    public Zone(String blocks) {
        try {
            this.blocks = Integer.valueOf(blocks);
        } catch (NumberFormatException e){

        }
    }

    public int getBlocks() {
        return blocks;
    }

    public void setBlocks(int blocks) {
        this.blocks = blocks;
    }

    public void validateCoverage(List<String> routes) throws ZoneException {
        int sum = 0;
        int singleRouteValidator = 0;
        String orientation = "N";
        for(String route: routes){

            for (int i=0; i < route.length(  ); i++) {
                char character = route.charAt(i);

                if (character == 'A') {
                    sum = (orientation.equals("N") || orientation.equals("E")) ? sum + 1 : sum - 1;
                    singleRouteValidator = sum;
                    singleRouteValidator *= (singleRouteValidator < 0) ? -1 : 1;
                    if (singleRouteValidator > this.blocks) throw new ZoneException(" Routes Exceeds number of Blocks");
                } else {
                    orientation = Util.getOrientation(character,orientation);
                }
            }
        }
        sum *= (sum < 0) ? -1 : 1;
        if (sum > this.blocks) throw new ZoneException(" Routes Exceeds number of Blocks");

    }
}
