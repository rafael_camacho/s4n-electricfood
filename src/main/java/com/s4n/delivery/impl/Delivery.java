package com.s4n.delivery.impl;

import com.s4n.delivery.*;

import java.util.List;
import java.util.Properties;

public class Delivery implements Runnable {

    private IInputFileProcessor iInputFileProcessor;

    private IOutputFileProcessor iOputFileProcessor;

    private Properties properties;

    private Drone drone;

    private int droneNumber;

    private IZone iZone;

    public Delivery(IInputFileProcessor iInputFileProcessor, IOutputFileProcessor iOutputFileProcessor, IZone iZone, Properties properties, int droneNumber) {
        this.iInputFileProcessor = iInputFileProcessor;
        this.iOputFileProcessor = iOutputFileProcessor;
        this.iZone = iZone;
        this.properties = properties;
        this.droneNumber = droneNumber;
    }

    public void executeDelivery() {
        this.run();
    }

    @Override
    public void run() {
        try {
            this.drone = this.iInputFileProcessor.readFile(properties.getProperty("input.file.prefix"), properties.
                    getProperty("file.extension"),this.droneNumber,Integer.valueOf(properties.getProperty("delivery.number.by.drone")));
            this.iZone.validateCoverage(this.drone.getRoutes());
            List<String> deliveryReport = this.drone.executeRoute();
            this.iOputFileProcessor.writeDelivery(deliveryReport,properties.getProperty("output.file.prefix"),properties.getProperty("file.extension"), this.droneNumber);
        } catch (FileProcessorException e) {
            e.printStackTrace();
        } catch (ZoneException e) {
            e.printStackTrace();
        } catch (DroneException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
