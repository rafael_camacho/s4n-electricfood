package com.s4n.delivery.impl;

import com.s4n.delivery.FileProcessorException;
import com.s4n.delivery.IOutputFileProcessor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class OutputFileProcessor implements IOutputFileProcessor {

    @Override
    public void writeDelivery(List<String> deliveryReport,String filePrefix, String fileExtension, int droneNumber) throws FileProcessorException{
        String currentDir = System.getProperty("user.dir");
        StringBuilder filePath = new StringBuilder().append(currentDir).append("/").append(filePrefix).
                append(droneNumber).append(fileExtension);
        File file = new File(filePath.toString());
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write("== Reporte de entregas ==");
            writer.write("\n");
            writer.write("\n");
            for (String report : deliveryReport ){
                writer.write(report);
                writer.write("\n");
            }
            writer.close();
        } catch (IOException e) {
            throw new FileProcessorException("Error creating report for Drone number: " + droneNumber,e);
        }
    }
}
