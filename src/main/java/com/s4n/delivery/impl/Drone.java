package com.s4n.delivery.impl;

import com.s4n.delivery.DroneException;
import com.s4n.delivery.IDrone;
import com.s4n.delivery.util.Util;

import java.util.ArrayList;
import java.util.List;

public class Drone implements IDrone {

    List<String> routes;

    private int maxDeliveryNumber;

    public Drone (){}

    public Drone(List<String> routes, int maxDeliveryNumber) {
        this.routes = routes;
        this.maxDeliveryNumber = maxDeliveryNumber;
    }

    public int getMaxDeliveryNumber() {
        return maxDeliveryNumber;
    }

    public void setMaxDeliveryNumber(int maxDeliveryNumber) {
        this.maxDeliveryNumber = maxDeliveryNumber;
    }

    public List<String> getRoutes() {
        return routes;
    }

    public void setRoutes(List<String> routes) {
        this.routes = routes;
    }

    public List<String> executeRoute() throws DroneException {

        if (this.routes.size() > this.maxDeliveryNumber){
            throw new DroneException("programmed deliveries exceed the capacity of the drone");
        }

        List<String> deliveryReport = new ArrayList();
        int y =0;
        int x =0;
        String orientation = "N";
        String tempReport;
        for(String route: this.routes){

            for (int i=0; i < route.length(  ); i++) {
                char character = route.charAt(i);

                if (character == 'A') {
                    y = (orientation.equals("N")) ? y + 1 : y;
                    x = (orientation.equals("O")) ? x - 1 : x;
                    y = (orientation.equals("S")) ? y - 1 : y;
                    x = (orientation.equals("E")) ? x + 1 : x;
                } else {
                    orientation = Util.getOrientation(character,orientation);
                }
            }
            StringBuilder tempString = new StringBuilder().append("(").append(x).append(",").append(y).append(")")
                    .append(" ").append(Util.getFullOrientation(orientation));
            tempReport = tempString.toString();
            deliveryReport.add(tempReport);
        }
        return deliveryReport;
    }


}
