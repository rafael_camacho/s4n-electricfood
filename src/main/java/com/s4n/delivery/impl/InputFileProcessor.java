package com.s4n.delivery.impl;

import com.s4n.delivery.FileProcessorException;
import com.s4n.delivery.IInputFileProcessor;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputFileProcessor implements IInputFileProcessor {

    private final static Pattern pattern = Pattern.compile("\\s+");

    public InputFileProcessor() {
    }

    public Drone readFile(String filePrefix, String fileExtension, int droneNumber, int maxDeliveryNumber) throws FileProcessorException {
        String currentDir = System.getProperty("user.dir");
        Drone drone = new Drone();
        try {
            StringBuilder filePath = new StringBuilder().append(currentDir).append("/").append(filePrefix).
                    append(droneNumber).append(fileExtension);
            List<String> routes = Files.lines(Paths.get(filePath.toString()), Charset.defaultCharset())
                    .map(line -> pattern.split(line, 2)[0])
                    .collect(Collectors.toList());
            drone = new Drone(routes,maxDeliveryNumber);
        } catch (NoSuchFileException e){
            throw new FileProcessorException("There aren't a Delivery File for Drone Number:" + droneNumber,e);
        }
        catch (IOException io) {
            io.printStackTrace();
        }

        return drone;
    }
}
