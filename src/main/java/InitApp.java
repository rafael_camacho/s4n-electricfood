import com.s4n.delivery.config.DeliveryPropertyValues;
import com.s4n.delivery.impl.*;

import java.io.IOException;

public class InitApp {

    public static void main(String[] args) throws IOException {
        DeliveryPropertyValues deliveryPropertyValues = new DeliveryPropertyValues();
        Zone zone = new Zone(deliveryPropertyValues.getPropValues().getProperty("blocks.number"));
        int droneFleet = Integer.valueOf(deliveryPropertyValues.getPropValues().getProperty("drone.fleet.number"));
        for (int droneNumber=1; droneNumber<= droneFleet; droneNumber++){
            Delivery delivery = new Delivery(new InputFileProcessor(), new OutputFileProcessor(), zone,
                    deliveryPropertyValues.getPropValues(), droneNumber);
            delivery.executeDelivery();
        }

    }

}
